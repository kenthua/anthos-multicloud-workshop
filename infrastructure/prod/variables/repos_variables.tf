variable "platform_admins" { default = "platform-admins" }
variable "acm" { default = "anthos-config-management" }
variable "online_boutique_group" { default = "online-boutique" }
variable "online_boutique_project" { default = "online-boutique" }
