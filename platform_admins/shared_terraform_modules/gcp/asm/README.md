<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| asm\_version |  | string | n/a | yes |
| eks\_eip\_list |  | string | n/a | yes |
| eks\_ingress\_ip\_list |  | string | n/a | yes |
| eks\_list |  | string | n/a | yes |
| gke\_list |  | string | n/a | yes |
| gke\_location\_list |  | string | n/a | yes |
| gke\_net |  | string | n/a | yes |
| project\_id |  | string | n/a | yes |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->